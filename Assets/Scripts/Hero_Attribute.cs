﻿using UnityEngine;
using System.Collections;

public class Hero_Attribute : MonoBehaviour {

	public int HeroLevel = 1;
	public int BaseStrength = 17;
	private float StrengthGrow = 1.7f;
	public int BaseAgility = 14;
	private float AgilityGrow = 1.9f;
	public int BaseIntelligence = 16;
	private float IntelligenceGrow = 4;
}
