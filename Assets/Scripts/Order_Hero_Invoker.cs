﻿using UnityEngine;
using System.Collections;

public class Order_Hero_Invoker : MonoBehaviour {

	public GameObject BaseAttackParticle;
	public Transform LauncherPosition;
	private bool CanAttack  = false;


	GameObject Enemy = null;

	private void CheckCanAttack (){
		if (Enemy) {
			CanAttack = true;
		} else {
			CanAttack = false;
		}
	}

	public void BaseAttack(){
		if(CanAttack){
			LaunchAttackParticle();
		}
	}

	void AssignTarget(GameObject PickedByMouse){
		Enemy = PickedByMouse;
	}

	public void LaunchAttackParticle(){
		Instantiate (BaseAttackParticle, LauncherPosition.position, Quaternion.identity);
	}

}
