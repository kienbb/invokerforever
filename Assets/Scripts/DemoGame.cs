﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class DemoGame : MonoBehaviour {


	public void ResetLevel(){
		SceneManager.LoadScene (SceneManager.GetActiveScene().name);
	}

	public void QuitGame(){
		Application.Quit ();
	}
}
