﻿// Code auto-converted by Control Freak 2 on Monday, June 27, 2016!
using UnityEngine;
using System.Collections;

public class FPSController : MonoBehaviour
{

    public Animator CamAnimator, WeaponAnimator;

    public float moveSpeed;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update ()
	{
        CamAnimator.SetBool("Running", ControlFreak2.CF2Input.GetKey(KeyCode.W));
        WeaponAnimator.SetBool("Fire", ControlFreak2.CF2Input.GetKey(KeyCode.Space));

        if( ControlFreak2.CF2Input.GetKey(KeyCode.W))
        {
            transform.position = transform.position + transform.forward*moveSpeed*Time.deltaTime;
        }
    }
}
